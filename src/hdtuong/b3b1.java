package hdtuong;

public class b3b1 {
    public int student_id;
    public String student_name;
    public String student_address;
    public int student_diema1;
    public int student_diema2;

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getStudent_address() {
        return student_address;
    }

    public void setStudent_address(String student_address) {
        this.student_address = student_address;
    }

    public int getStudent_diema1() {
        return student_diema1;
    }

    public void setStudent_diema1(int student_diema1) {
        this.student_diema1 = student_diema1;
    }

    public int getStudent_diema2() {
        return student_diema2;
    }

    public void setStudent_diema2(int student_diema2) {
        this.student_diema2 = student_diema2;
    }
}
