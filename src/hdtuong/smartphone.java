package hdtuong;

public class smartphone extends b4hdt{
    private int camera;
    private String security;
    public smartphone(String namePhone,String display,int battery,int camera,String security ){
        super(namePhone,display,battery);
        this.camera= camera;
        this.security= security;
    }
    public int getCamera() {
        return camera;
    }
    public void setCamera(int camera) {
        this.camera = camera;
    }
    public String getSecurity(){
        return security;
    }
    public void setSecurity(String security) {
        this.security = security;
    }
    @Override
    public void infoPhone(){
        System.out.println("Name:"+getNamePhone());
        System.out.println("Display:"+getDisplay());
        System.out.println("Battery:"+getBattery());
        System.out.println("Camera:"+this.camera);
        System.out.println("Security:"+this.security);
    }
}
