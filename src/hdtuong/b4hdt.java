package hdtuong;

public class b4hdt {
    private String namePhone;
    private String display;
    private int battery;

    public String getNamePhone() {
        return namePhone;
    }

    public void setNamePhone(String namePhone) {
        this.namePhone = namePhone;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }
    public b4hdt(String namePhone, String display,int battery){//constructor:khai bao gtri khoi tao
        this.namePhone=namePhone;
        this.display=display;
        this.battery=battery;
    }
    public void infoPhone(){
        System.out.println("Name:"+this.namePhone);
        System.out.println("Display:"+this.display);
        System.out.println("Battery:"+this.battery);
    }
}
