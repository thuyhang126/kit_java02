package hdtuong;

public class b3b3 {
    public String sinhvien_name;
    public String sinhvien_address;
    public float sinhvien_diema1;
    public float sinhvien_diema2;
    public float sinhvien_diema3;
    public float sinhvien_knm;
    public float sinhvien_ktlt;
    public String getSinhvien_name() {
        return sinhvien_name;
    }

    public void setSinhvien_name(String sinhvien_name) {
        this.sinhvien_name = sinhvien_name;
    }

    public String getSinhvien_address() {
        return sinhvien_address;
    }

    public void setSinhvien_address(String sinhvien_address) {
        this.sinhvien_address = sinhvien_address;
    }

    public float getSinhvien_diema1() {
        return sinhvien_diema1;
    }

    public void setSinhvien_diema1(float sinhvien_diema1) {
        this.sinhvien_diema1 = sinhvien_diema1;
    }

    public float getSinhvien_diema2() {
        return sinhvien_diema2;
    }

    public void setSinhvien_diema2(float sinhvien_diema2) {
        this.sinhvien_diema2 = sinhvien_diema2;
    }

    public float getSinhvien_diema3() {
        return sinhvien_diema3;
    }

    public void setSinhvien_diema3(float sinhvien_diema3) {
        this.sinhvien_diema3 = sinhvien_diema3;
    }

    public float getSinhvien_knm() {
        return sinhvien_knm;
    }

    public void setSinhvien_knm(float sinhvien_knm) {
        this.sinhvien_knm = sinhvien_knm;
    }

    public float getSinhvien_ktlt() {
        return sinhvien_ktlt;
    }

    public void setSinhvien_ktlt(float sinhvien_ktlt) {
        this.sinhvien_ktlt = sinhvien_ktlt;
    }



}
