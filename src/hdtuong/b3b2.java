package hdtuong;

public class b3b2 {
    public int hanghoa_ma;
    public String hanghoa_ten;
    public float hanghoa_giamuavao;
    public float hanghoa_giabanra;
    public int hanghoa_soluong;

    public int getHanghoa_ma() {
        return hanghoa_ma;
    }

    public float getHanghoa_giamuavao() {
        return hanghoa_giamuavao;
    }

    public void setHanghoa_giamuavao(float hanghoa_giamuavao) {
        this.hanghoa_giamuavao = hanghoa_giamuavao;
    }

    public float getHanghoa_giabanra() {
        return hanghoa_giabanra;
    }

    public void setHanghoa_giabanra(float hanghoa_giabanra) {
        this.hanghoa_giabanra = hanghoa_giabanra;
    }

    public int getHanghoa_soluong() {
        return hanghoa_soluong;
    }

    public void setHanghoa_soluong(int hanghoa_soluong) {
        this.hanghoa_soluong = hanghoa_soluong;
    }

    public void setHanghoa_ma(int hanghoa_ma) {
        this.hanghoa_ma = hanghoa_ma;

    }
}
